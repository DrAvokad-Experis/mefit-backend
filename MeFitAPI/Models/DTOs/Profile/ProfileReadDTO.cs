namespace MeFitAPI.Models.DTOs.Profile
{
    public class ProfileReadDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Height { get; set; }
        public int Weight { get; set; }
        // Relationships 
        public string UserId { get; set; }
        public int Address { get; set; }
        public int Goal { get; set; }
        public ICollection<int> Programs { get; set; }
        public ICollection<int> Workouts { get; set; }
        public ICollection<int> Exercises { get; set; }
        public int Set { get; set; }
        public ICollection<int> Categories { get; set;}

    }
}
