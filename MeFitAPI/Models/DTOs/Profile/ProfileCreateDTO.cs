﻿namespace MeFitAPI.Models.DTOs.Profile
{
    public class ProfileCreateDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Height { get; set; }
        public int Weight { get; set; }
        public string UserId { get; set; }
    }
}
