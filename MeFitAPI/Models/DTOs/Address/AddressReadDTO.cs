﻿namespace MeFitAPI.Models.DTOs.Address
{
    public class AddressReadDTO
    {
        public int Id { get; set; }
        public string AddressLine1 { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public int Profile { get; set; }
    }
}
