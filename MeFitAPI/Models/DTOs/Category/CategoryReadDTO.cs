﻿namespace MeFitAPI.Models.DTOs.Category
{
    public class CategoryReadDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        // Relationships
        public ICollection<int> Profiles { get; set; }
    }
}
