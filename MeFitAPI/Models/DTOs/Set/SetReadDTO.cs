﻿namespace MeFitAPI.Models.DTOs.Set
{
    public class SetReadDTO
    {
        public int Id { get; set; }
        public int ExerciseRepetitions { get; set; }
        public int Exercise { get; set; }
    }
}
