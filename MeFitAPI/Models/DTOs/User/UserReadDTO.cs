﻿namespace MeFitAPI.Models.DTOs.User
{
    public class UserReadDTO
    {
        public int Id { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        // Relationships
        public int Profile { get; set; }
    }
}
