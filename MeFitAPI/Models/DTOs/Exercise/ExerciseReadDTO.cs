﻿namespace MeFitAPI.Models.DTOs.Exercise
{
    public class ExerciseReadDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string TargetMuscleGroup { get; set; }
        public string Img { get; set; }
        public string VideoLink { get; set; }
        public ICollection<int> Workouts { get; set; }
        public ICollection<int> Sets { get; set; }
        public int? Profile { get; set; }
    }
}
