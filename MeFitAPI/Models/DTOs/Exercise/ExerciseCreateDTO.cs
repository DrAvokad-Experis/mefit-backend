﻿namespace MeFitAPI.Models.DTOs.Exercise
{
    public class ExerciseCreateDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string TargetMuscleGroup { get; set; }
        public string Img { get; set; }
        public string VideoLink { get; set; }
        public int? Profile { get; set; }
    }
}
