﻿namespace MeFitAPI.Models.DTOs.GymProgram
{
    public class GymProgramEditDTO
    {
        // Fields
        public int Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
    }
}
