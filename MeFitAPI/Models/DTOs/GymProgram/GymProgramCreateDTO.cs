﻿namespace MeFitAPI.Models.DTOs.GymProgram
{
    public class GymProgramCreateDTO
    {
        // Fields
        public string Name { get; set; }
        public string Category { get; set; }
        public int? Profile { get; set; }
    }
}
