﻿namespace MeFitAPI.Models.DTOs
{
    public class GymProgramReadDTO
    {
        // Fields
        public int Id { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        // Relationships
        public List<int> Workouts { get; set; }
        public int? Profile { get; set; }
    }
}
