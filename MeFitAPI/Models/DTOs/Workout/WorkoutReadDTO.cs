﻿namespace MeFitAPI.Models.DTOs.Workout
{
    public class WorkoutReadDTO
    {
        // Fields
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public bool Complete { get; set; }
        // Relationships
        public ICollection<int> Programs { get; set; }
        public ICollection<int> Exercises { get; set; }
        public int Set { get; set; }
        public int? Profile { get; set; }
    }
}
