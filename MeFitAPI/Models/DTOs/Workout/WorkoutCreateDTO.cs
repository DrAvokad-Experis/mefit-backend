﻿namespace MeFitAPI.Models.DTOs.Workout
{
    public class WorkoutCreateDTO
    {
        // Fields
        public string Name { get; set; }
        public string Type { get; set; }
        public bool Complete { get; set; }
        public int? Profile { get; set; }
    }
}
