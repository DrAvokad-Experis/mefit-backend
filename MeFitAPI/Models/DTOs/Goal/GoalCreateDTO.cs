﻿namespace MeFitAPI.Models.DTOs.Goal
{
    public class GoalCreateDTO
    {
        // Fields
        public DateTime EndDate { get; set; }
        public bool Achieved { get; set; }
    }
}
