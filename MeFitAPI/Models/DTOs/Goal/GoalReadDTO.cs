﻿namespace MeFitAPI.Models.DTOs.Goal
{
    public class GoalReadDTO
    {
        public int Id { get; set; }
        public DateTime EndDate { get; set; }
        public bool Achieved { get; set; }
        // Relationships
        public int Program { get; set; }
        public ICollection<int> Workouts { get; set; }
    }
}
