﻿using System.ComponentModel.DataAnnotations;

namespace MeFitAPI.Models.Domain
{
    public class Category
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(100)]
        public string Name { get; set; }
        // Relationships
        public ICollection<ProfileModel> Profiles { get; set; }
    }
}
