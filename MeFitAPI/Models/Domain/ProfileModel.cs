﻿using System.ComponentModel.DataAnnotations;

namespace MeFitAPI.Models.Domain
{
    public class ProfileModel
    {
        // Fields
        [Key]
        public int Id { get; set; }
        [MaxLength(100)]
        public string? FirstName { get; set; }
        [MaxLength(100)]
        public string? LastName { get; set; }
        public int? Height { get; set; }
        public int? Weight { get; set; }
        public int? GoalId { get; set; }
        // Relationships
        [MaxLength(500)]
        public string? UserId { get; set; }
        public int? AddressId { get; set; }
        public Address Address { get; set; }
        public Goal Goal { get; set; }
        public ICollection<GymProgram>? GymPrograms { get; set; }
        public ICollection<Workout>? Workouts { get; set; }
        public ICollection<Exercise>? Exercises { get; set; }
        public int? SetId { get; set; }
        public Set Set { get; set; }
        public ICollection<Category> Categories { get; set; }
    }
}
