﻿using System.ComponentModel.DataAnnotations;

namespace MeFitAPI.Models.Domain
{
    public class Set
    {
        [Key]
        public int Id { get; set; }
        public int? ExerciseRepetitions { get; set; }
        // Relationships
        public int? ExerciseId { get; set; }
        public Exercise Exercise { get; set; }
        public Workout Workout { get; set; }
    }
}
