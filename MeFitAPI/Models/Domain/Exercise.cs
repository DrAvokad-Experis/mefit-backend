﻿using System.ComponentModel.DataAnnotations;

namespace MeFitAPI.Models.Domain
{
    public class Exercise
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        [MaxLength(500)]
        public string? Description { get; set; }
        [MaxLength(100)]
        public string? TargetMuscleGroup { get; set; }
        [MaxLength(300)]
        public string? Img { get; set; }
        [MaxLength(300)]
        public string? VideoLink { get; set; }
        public ICollection<Workout> Workouts { get; set; }
        public ICollection<Set> Sets { get; set; }
        public int? ProfileId { get; set; }
        public ProfileModel Profile { get; set; }
    }
}
