﻿using System.ComponentModel.DataAnnotations;

namespace MeFitAPI.Models.Domain
{
    public class GymProgram
    {
        // Fields
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        [MaxLength(100)]
        public string? Category { get; set; }
        // Relationships
        public ICollection<Workout> Workouts { get; set; }
        public ICollection<Goal> Goals { get; set; }
        public int? ProfileId { get; set; }
        public ProfileModel Profile { get; set; }
    }
}
