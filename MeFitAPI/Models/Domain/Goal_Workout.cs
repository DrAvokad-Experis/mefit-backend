﻿namespace MeFitAPI.Models.Domain
{
    public class Goal_Workout
    {
        public int Id { get; set; }
        public int GoalId { get; set; }
        public Goal Goal { get; set; }
        public int WorkoutId { get; set; }
        public Workout Workout { get; set; }
        public bool Achieved { get; set; }
    }
}
