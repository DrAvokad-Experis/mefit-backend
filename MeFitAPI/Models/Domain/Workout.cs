﻿using System.ComponentModel.DataAnnotations;

namespace MeFitAPI.Models.Domain
{
    public class Workout
    {
        // Fields
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }
        [Required]
        [MaxLength(100)]
        public string? Type { get; set; }
        public bool? Complete { get; set; }
        // Relationships
        public ICollection<GymProgram> Programs { get; set; }
        public ICollection<Exercise> Exercises { get; set; }
        public ICollection<Goal> Goals { get; set; }
        public int? SetId { get; set; }
        public Set? Set { get; set; }
        public int? ProfileId { get; set; }
        public ProfileModel Profile { get; set; }
    }
}
