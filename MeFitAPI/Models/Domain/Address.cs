﻿using MeFitAPI.Models.Domain;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MeFitAPI.Models
{
    public class Address
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(100)]
        public string AddressLine1 { get; set; }
        [MaxLength(25)]
        public string PostalCode { get; set; }
        [MaxLength(100)]
        public string City { get; set; }
        [MaxLength(50)]
        public string Country { get; set; }
        public ProfileModel Profile { get; set; }
    }
}
