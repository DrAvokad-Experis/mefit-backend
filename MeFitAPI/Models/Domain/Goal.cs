﻿using System.ComponentModel.DataAnnotations;

namespace MeFitAPI.Models.Domain
{
    public class Goal
    {
        // Fields
        [Key]
        public int Id { get; set; }
        [Required]
        public DateTime EndDate { get; set; }
        public bool? Achieved { get; set; }
        // Relationships
        public int? ProgramId { get; set; }
        public GymProgram Program { get; set; }
        public ICollection<Workout> Workouts { get; set; } = new List<Workout>();
    }
}
