﻿using System.ComponentModel.DataAnnotations;

namespace MeFitAPI.Models.Domain
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        [Required]
        [MaxLength(25)]
        public string Username { get; set; }
        [Required]
        [MaxLength(50)]
        public string Password { get; set; }
        // Relationships
    }
}
