﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MeFitAPI.Migrations
{
    public partial class addGoalProgramRelationship : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ProgramId",
                table: "Goals",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Goals_ProgramId",
                table: "Goals",
                column: "ProgramId");

            migrationBuilder.AddForeignKey(
                name: "FK_Goals_Programs_ProgramId",
                table: "Goals",
                column: "ProgramId",
                principalTable: "Programs",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Goals_Programs_ProgramId",
                table: "Goals");

            migrationBuilder.DropIndex(
                name: "IX_Goals_ProgramId",
                table: "Goals");

            migrationBuilder.DropColumn(
                name: "ProgramId",
                table: "Goals");
        }
    }
}
