﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MeFitAPI.Migrations
{
    public partial class removeRelationships : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Workouts_Programs_GymProgramId",
                table: "Workouts");

            migrationBuilder.DropIndex(
                name: "IX_Workouts_GymProgramId",
                table: "Workouts");

            migrationBuilder.DropColumn(
                name: "GymProgramId",
                table: "Workouts");

            migrationBuilder.DropColumn(
                name: "SetId",
                table: "Workouts");

            migrationBuilder.DropColumn(
                name: "GoalId",
                table: "Profiles");

            migrationBuilder.DropColumn(
                name: "ProgramId",
                table: "Profiles");

            migrationBuilder.DropColumn(
                name: "UserId",
                table: "Profiles");

            migrationBuilder.DropColumn(
                name: "WorkoutId",
                table: "Profiles");

            migrationBuilder.DropColumn(
                name: "ProgramId",
                table: "Goals");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "GymProgramId",
                table: "Workouts",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SetId",
                table: "Workouts",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "GoalId",
                table: "Profiles",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ProgramId",
                table: "Profiles",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "UserId",
                table: "Profiles",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "WorkoutId",
                table: "Profiles",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ProgramId",
                table: "Goals",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Workouts_GymProgramId",
                table: "Workouts",
                column: "GymProgramId");

            migrationBuilder.AddForeignKey(
                name: "FK_Workouts_Programs_GymProgramId",
                table: "Workouts",
                column: "GymProgramId",
                principalTable: "Programs",
                principalColumn: "Id");
        }
    }
}
