﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MeFitAPI.Migrations
{
    public partial class noRelationships : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Programs",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Programs",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Programs",
                keyColumn: "Id",
                keyValue: 3);

            migrationBuilder.DeleteData(
                table: "Workouts",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "Workouts",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DeleteData(
                table: "Workouts",
                keyColumn: "Id",
                keyValue: 3);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "Exercises",
                columns: new[] { "Id", "Description", "Img", "Name", "TargetMuscleGroup", "VideoLink" },
                values: new object[,]
                {
                    { 1, "Lie down on a bench and press a barbell upwards from your chest.", "benchpress.jpg", "Bench Press", "Chest", "Https://youtube.com/benchpress" },
                    { 2, "Stand up and pull a weighted barbell upwards.", "Deadlift.jpg", "Deadlift", "Hamstrings", null },
                    { 3, "Put a barbell on your scapulas and bend the knee to the king", null, "Squat", "Glutes", null }
                });

            migrationBuilder.InsertData(
                table: "Programs",
                columns: new[] { "Id", "Category", "Name" },
                values: new object[,]
                {
                    { 1, "Arms", "Tricep Trifecta" },
                    { 2, "Chest", "Booba Blowup" },
                    { 3, "Lower Body", "Lovely Legs" }
                });

            migrationBuilder.InsertData(
                table: "Workouts",
                columns: new[] { "Id", "Complete", "Name", "Type" },
                values: new object[,]
                {
                    { 1, null, "Back/Biceps", "Hypertrophy" },
                    { 2, null, "Treadmill HIIT", "Cardiovascular" },
                    { 3, null, "Squat 1RM", "Strength Training" }
                });
        }
    }
}
