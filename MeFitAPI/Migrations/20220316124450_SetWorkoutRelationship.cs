﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MeFitAPI.Migrations
{
    public partial class SetWorkoutRelationship : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SetId",
                table: "Workouts",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Workouts_SetId",
                table: "Workouts",
                column: "SetId",
                unique: true,
                filter: "[SetId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_Workouts_Sets_SetId",
                table: "Workouts",
                column: "SetId",
                principalTable: "Sets",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Workouts_Sets_SetId",
                table: "Workouts");

            migrationBuilder.DropIndex(
                name: "IX_Workouts_SetId",
                table: "Workouts");

            migrationBuilder.DropColumn(
                name: "SetId",
                table: "Workouts");
        }
    }
}
