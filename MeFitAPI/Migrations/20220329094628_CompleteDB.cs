﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MeFitAPI.Migrations
{
    public partial class CompleteDB : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Profiles_Programs_ProgramId",
                table: "Profiles");

            migrationBuilder.DropForeignKey(
                name: "FK_Profiles_Workouts_WorkoutId",
                table: "Profiles");

            migrationBuilder.DropIndex(
                name: "IX_Profiles_ProgramId",
                table: "Profiles");

            migrationBuilder.DropIndex(
                name: "IX_Profiles_WorkoutId",
                table: "Profiles");

            migrationBuilder.DropColumn(
                name: "ProgramId",
                table: "Profiles");

            migrationBuilder.DropColumn(
                name: "WorkoutId",
                table: "Profiles");

            migrationBuilder.AddColumn<int>(
                name: "ProfileId",
                table: "Workouts",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ProfileId",
                table: "Programs",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ProfileId",
                table: "Exercises",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Categories",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(100)", maxLength: 100, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Categories", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "CategoryProfileModel",
                columns: table => new
                {
                    CategoriesId = table.Column<int>(type: "int", nullable: false),
                    ProfilesId = table.Column<int>(type: "int", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CategoryProfileModel", x => new { x.CategoriesId, x.ProfilesId });
                    table.ForeignKey(
                        name: "FK_CategoryProfileModel_Categories_CategoriesId",
                        column: x => x.CategoriesId,
                        principalTable: "Categories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CategoryProfileModel_Profiles_ProfilesId",
                        column: x => x.ProfilesId,
                        principalTable: "Profiles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Exercises",
                columns: new[] { "Id", "Description", "Img", "Name", "ProfileId", "TargetMuscleGroup", "VideoLink" },
                values: new object[,]
                {
                    { 4, "Grip a barbell with your elbows with as much weight as you can possibly hold then shrug your shoulders. Grunt as loudly as possible during the exercise and finish off by screaming your lungs out", "https://upload.wikimedia.org/wikipedia/commons/3/3a/Grizzly_Denali_Cop.jpg", "Zercher Shrugs", null, "Trapezius", "https://youtu.be/1LsIQr_4iSY" },
                    { 5, "Hold a barbell over the end of your spine, resting your hands on the top of your glutes, then curl your hands upwards. Load barbell with as much weight as possible", "https://upload.wikimedia.org/wikipedia/commons/5/59/Flexor-carpi-radialis.png", "Standing Forearm Curls", null, "Forearms", "https://youtu.be/zcge1HFwpfw" },
                    { 6, "Load a barbell with adequate weight and hold it with your elbows. Then proceed to squat down until your knees are at a 90 degree angle, then press yourself back up", "https://upload.wikimedia.org/wikipedia/commons/1/16/Zecher-squats-1.gif", "Zercher Squat", null, "Quardriceps", "https://youtu.be/SQTMbyFkcRI" },
                    { 7, "Load a barbell with adequate weight and pick it up from the ground with your elbows. Lift the barbell to a standing position, then lower the barbell back to the ground", "Zercher.png", "Zercher Deadlift", null, "Glutes", "https://youtu.be/93Ef0C8mg5U" },
                    { 8, "From a standing position press a barbell from your chest to above your head", "https://upload.wikimedia.org/wikipedia/commons/5/5f/Lasha_Talakhadze_Rio_2016.jpg", "Military Press", null, "Deltoids", "https://youtu.be/ryi5s0a9ZJQ" },
                    { 9, "Lie down on a bench with the backrest angled up at about a 35 degrees and press a barbell upwards from your chest.", "inclinepress.jpg", "Incline Benchpress", null, "Pectorals", "https://youtu.be/67zS5weDiXQ" },
                    { 10, "Lie down on a flat bench with the seat elevated higher thatn the backrest and press a barbell upwards from your chest.", "declinepress.png", "Decline Benchpress", null, "Pectorals", "https://youtu.be/QS2qQZsxBe0" },
                    { 11, "Lie down on a flat bench and press a dumbbell in each hand upwards from your chest.", "https://upload.wikimedia.org/wikipedia/commons/2/2e/Dumbbell-bench-press-1.png", "Dumbbell Press", null, "Pectorals", "https://youtu.be/jRUC6IVav30" },
                    { 12, "Lie down on a bench with the backrest angled up at about a 35 degrees and press a dumbbell in each hand upwards from your chest", "https://upload.wikimedia.org/wikipedia/commons/3/3d/Incline-press-2.png", "Incline Dumbbell Press", null, "Pectorals", "https://youtu.be/wpBTgVfPq28" },
                    { 13, "From a standing position,  press a dumbbell up over your head", "https://upload.wikimedia.org/wikipedia/commons/f/f2/One-arm-shoulder-press-1.png", "Overhead Dumbell Press", null, "Deltoids", "https://youtu.be/6pSRdyUTsPs" },
                    { 14, "Grip the handles of the machine and press them forwards", "https://upload.wikimedia.org/wikipedia/commons/4/48/Incline-bench-press-2-1.png", "Machine Chest Press", null, "Pectorals", "https://youtu.be/2y6ntGVg4dw" },
                    { 15, "Grip a fastened pull-up bar with your elbows and somehow manage to pull yourself from the ground", "madness.jpg", "Zercher Pull-up", null, "Unknown", "zercher.mp4" },
                    { 16, "Grip a barbell with the crooks of your elbows and curl it", "zerchercurl.svg", "Zercher Curls", null, "Biceps", "advancedcurling.mp4" },
                    { 17, "The hack squat involves standing on the plate of a machine, leaning back onto its the pads at an angle, with the weight placed on top of you by positioning yourself under the shoulder pads. The weight is then pushed in the concentric phase of the squat", "https://upload.wikimedia.org/wikipedia/commons/9/98/Hack-squats-2.gif", "Hacksquat", null, "Glutes", "https://youtu.be/dWtaVd_XmX4" },
                    { 18, "Put a barbell on top of the front of your shoulders and squat down to a 90 degree angle and then stand back up.", "https://upload.wikimedia.org/wikipedia/commons/0/02/Front-squat-2-857x1024.png", "Front Squat", null, "Quadriceps", "https://youtu.be/iytyDf-yJZs" },
                    { 19, "Put weight on the back of your ankle with the help of a machine and curl your leg upwards", "https://upload.wikimedia.org/wikipedia/commons/f/fe/Standing-leg-curl-1.png", "Leg Curl", null, "Hamstrings", "https://youtu.be/d4PQv5D06_Y" },
                    { 20, " During the clean, the lifter moves the barbell from the floor to a racked position across the deltoids, without resting fully on the clavicles. During the jerk, the lifter raises the barbell to a stationary position above the head, finishing with straight arms and legs, and the feet in the same plane as the torso and barbell.", "https://upload.wikimedia.org/wikipedia/commons/a/af/EVD-pesas-056.jpg", "Clean & Jerk", null, "Full Body", "https://youtu.be/IIHZUR67JsY" },
                    { 21, "Even if you could curl a barbell any other place in the gym, purposefully curl in the squat rack to show your dominance over all other gym-goers and your disdain of leg day", "bro.png", "Curls in the Squat Rack", null, "Ego", "brosciencelife.mp4" },
                    { 22, "Occupy the squat rack by flexing, boosting your ego and blocking any attempts at leg workouts", "dom.png", "Flex in the Squat Rack", null, "Ego", "https://youtube.com/brosciencelife" },
                    { 23, "After you have proven to everyone how much you can bench proceed to get a nice arm-pump by curling the barbell", "mazetti.jpg", "Curl At the Bench", null, "Ego", "alpha.mp4" },
                    { 24, "Grip a dumbbell and curl it bro", "https://upload.wikimedia.org/wikipedia/commons/c/c6/Two-arm-preacher-curl-2.gif", "Bicep Curls", null, "Biceps", "https://youtube.com/shorts/q99mHEoIw-Q?feature=share" },
                    { 25, "This curl is performed simultaneously with dumbbells but without wrist supination. Throughout each rep, the wrists remain neutral, like a carpenter hammering a nail.", "hammercurl.png", "Hammer Curls", null, null, "https://youtu.be/WGTCVgCqLqM" },
                    { 26, "Hoist yourself violently up a pull-up bar swinging back and forth wildly. Exit the movement by hurling yourself face-first into the ground", "https://upload.wikimedia.org/wikipedia/commons/c/ca/Clown.svg", "Crossfit Pull-up", null, "Pain Receptors", "https://youtube.com/shorts/WfhQNv0otBQ?feature=share" },
                    { 27, "Just like a normal clean and jerk but try to injure yourself in the process", "https://upload.wikimedia.org/wikipedia/commons/c/ca/Clown.svg", "Crossfit Clean & Jerk", null, "Face", "https://youtube.com/shorts/Yt0NifZskeU?feature=share" }
                });

            migrationBuilder.UpdateData(
                table: "Programs",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Category", "Name" },
                values: new object[] { "FULL", "Zercher Positive" });

            migrationBuilder.UpdateData(
                table: "Programs",
                keyColumn: "Id",
                keyValue: 2,
                column: "Category",
                value: "Body-building - Intermediate");

            migrationBuilder.UpdateData(
                table: "Programs",
                keyColumn: "Id",
                keyValue: 3,
                column: "Category",
                value: "Body-building - Beginner");

            migrationBuilder.InsertData(
                table: "Programs",
                columns: new[] { "Id", "Category", "Name", "ProfileId" },
                values: new object[,]
                {
                    { 4, "Weightlifting - Advanced", "Very clean", null },
                    { 5, "Broscience", "Dom's Gauntlet", null },
                    { 6, "Stupidity", "Extreme Crossfit", null },
                    { 7, "Powerlifting - Beginner", "BDSM (Bench, Deadlift, Squat, Military-Press", null }
                });

            migrationBuilder.InsertData(
                table: "Workouts",
                columns: new[] { "Id", "Complete", "Name", "ProfileId", "SetId", "Type" },
                values: new object[,]
                {
                    { 4, null, "Kyriakos Classic", null, null, "Strength Training" },
                    { 5, null, "Zercher Suicide", null, null, "Strength Training" },
                    { 6, null, "Bench-impress", null, null, "Strength Training" },
                    { 7, null, "Dumbbell Pressout", null, null, "Hypertrophy" },
                    { 8, null, "Chest vs Machine", null, null, "Hypertrophy" },
                    { 9, null, "Partial Arts", null, null, "Technique" },
                    { 10, null, "Party in the Quad", null, null, "Hypertrophy" },
                    { 11, null, "Booty Builder", null, null, "Hypertrophy" },
                    { 12, null, "Squat Sesh", null, null, "Strength Training" },
                    { 13, null, "Mr.Clean", null, null, "Strength Training" },
                    { 14, null, "Squat-rack Alpha", null, null, "Technique" }
                });

            migrationBuilder.InsertData(
                table: "Workouts",
                columns: new[] { "Id", "Complete", "Name", "ProfileId", "SetId", "Type" },
                values: new object[,]
                {
                    { 15, null, "Own the Bench", null, null, "Technique" },
                    { 16, null, "Curls for Jesus", null, null, "Hypertrophy" },
                    { 17, null, "Barbell Mastery", null, null, "Technique" },
                    { 18, null, "Crazy Calisthenics", null, null, "Technique" },
                    { 19, null, "Unclean Cleans", null, null, "Technique" },
                    { 20, null, "Non-lethal Deadlifts", null, null, "Strength Training" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Workouts_ProfileId",
                table: "Workouts",
                column: "ProfileId");

            migrationBuilder.CreateIndex(
                name: "IX_Programs_ProfileId",
                table: "Programs",
                column: "ProfileId");

            migrationBuilder.CreateIndex(
                name: "IX_Exercises_ProfileId",
                table: "Exercises",
                column: "ProfileId");

            migrationBuilder.CreateIndex(
                name: "IX_CategoryProfileModel_ProfilesId",
                table: "CategoryProfileModel",
                column: "ProfilesId");

            migrationBuilder.AddForeignKey(
                name: "FK_Exercises_Profiles_ProfileId",
                table: "Exercises",
                column: "ProfileId",
                principalTable: "Profiles",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Programs_Profiles_ProfileId",
                table: "Programs",
                column: "ProfileId",
                principalTable: "Profiles",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Workouts_Profiles_ProfileId",
                table: "Workouts",
                column: "ProfileId",
                principalTable: "Profiles",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Exercises_Profiles_ProfileId",
                table: "Exercises");

            migrationBuilder.DropForeignKey(
                name: "FK_Programs_Profiles_ProfileId",
                table: "Programs");

            migrationBuilder.DropForeignKey(
                name: "FK_Workouts_Profiles_ProfileId",
                table: "Workouts");

            migrationBuilder.DropTable(
                name: "CategoryProfileModel");

            migrationBuilder.DropTable(
                name: "Categories");

            migrationBuilder.DropIndex(
                name: "IX_Workouts_ProfileId",
                table: "Workouts");

            migrationBuilder.DropIndex(
                name: "IX_Programs_ProfileId",
                table: "Programs");

            migrationBuilder.DropIndex(
                name: "IX_Exercises_ProfileId",
                table: "Exercises");

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 21);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 22);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 23);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 24);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 25);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 26);

            migrationBuilder.DeleteData(
                table: "Exercises",
                keyColumn: "Id",
                keyValue: 27);

            migrationBuilder.DeleteData(
                table: "Programs",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Programs",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Programs",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Programs",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Workouts",
                keyColumn: "Id",
                keyValue: 4);

            migrationBuilder.DeleteData(
                table: "Workouts",
                keyColumn: "Id",
                keyValue: 5);

            migrationBuilder.DeleteData(
                table: "Workouts",
                keyColumn: "Id",
                keyValue: 6);

            migrationBuilder.DeleteData(
                table: "Workouts",
                keyColumn: "Id",
                keyValue: 7);

            migrationBuilder.DeleteData(
                table: "Workouts",
                keyColumn: "Id",
                keyValue: 8);

            migrationBuilder.DeleteData(
                table: "Workouts",
                keyColumn: "Id",
                keyValue: 9);

            migrationBuilder.DeleteData(
                table: "Workouts",
                keyColumn: "Id",
                keyValue: 10);

            migrationBuilder.DeleteData(
                table: "Workouts",
                keyColumn: "Id",
                keyValue: 11);

            migrationBuilder.DeleteData(
                table: "Workouts",
                keyColumn: "Id",
                keyValue: 12);

            migrationBuilder.DeleteData(
                table: "Workouts",
                keyColumn: "Id",
                keyValue: 13);

            migrationBuilder.DeleteData(
                table: "Workouts",
                keyColumn: "Id",
                keyValue: 14);

            migrationBuilder.DeleteData(
                table: "Workouts",
                keyColumn: "Id",
                keyValue: 15);

            migrationBuilder.DeleteData(
                table: "Workouts",
                keyColumn: "Id",
                keyValue: 16);

            migrationBuilder.DeleteData(
                table: "Workouts",
                keyColumn: "Id",
                keyValue: 17);

            migrationBuilder.DeleteData(
                table: "Workouts",
                keyColumn: "Id",
                keyValue: 18);

            migrationBuilder.DeleteData(
                table: "Workouts",
                keyColumn: "Id",
                keyValue: 19);

            migrationBuilder.DeleteData(
                table: "Workouts",
                keyColumn: "Id",
                keyValue: 20);

            migrationBuilder.DropColumn(
                name: "ProfileId",
                table: "Workouts");

            migrationBuilder.DropColumn(
                name: "ProfileId",
                table: "Programs");

            migrationBuilder.DropColumn(
                name: "ProfileId",
                table: "Exercises");

            migrationBuilder.AddColumn<int>(
                name: "ProgramId",
                table: "Profiles",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "WorkoutId",
                table: "Profiles",
                type: "int",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "Programs",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Category", "Name" },
                values: new object[] { "Arms", "Tricep Trifecta" });

            migrationBuilder.UpdateData(
                table: "Programs",
                keyColumn: "Id",
                keyValue: 2,
                column: "Category",
                value: "Chest");

            migrationBuilder.UpdateData(
                table: "Programs",
                keyColumn: "Id",
                keyValue: 3,
                column: "Category",
                value: "Lower Body");

            migrationBuilder.CreateIndex(
                name: "IX_Profiles_ProgramId",
                table: "Profiles",
                column: "ProgramId");

            migrationBuilder.CreateIndex(
                name: "IX_Profiles_WorkoutId",
                table: "Profiles",
                column: "WorkoutId");

            migrationBuilder.AddForeignKey(
                name: "FK_Profiles_Programs_ProgramId",
                table: "Profiles",
                column: "ProgramId",
                principalTable: "Programs",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Profiles_Workouts_WorkoutId",
                table: "Profiles",
                column: "WorkoutId",
                principalTable: "Workouts",
                principalColumn: "Id");
        }
    }
}
