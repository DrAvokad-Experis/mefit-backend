﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MeFitAPI.Migrations
{
    public partial class profileRelationships : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "GoalId",
                table: "Profiles",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "ProgramId",
                table: "Profiles",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "SetId",
                table: "Profiles",
                type: "int",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "WorkoutId",
                table: "Profiles",
                type: "int",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "Set",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ExerciseRepetitions = table.Column<int>(type: "int", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Set", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Profiles_GoalId",
                table: "Profiles",
                column: "GoalId");

            migrationBuilder.CreateIndex(
                name: "IX_Profiles_ProgramId",
                table: "Profiles",
                column: "ProgramId");

            migrationBuilder.CreateIndex(
                name: "IX_Profiles_SetId",
                table: "Profiles",
                column: "SetId");

            migrationBuilder.CreateIndex(
                name: "IX_Profiles_WorkoutId",
                table: "Profiles",
                column: "WorkoutId");

            migrationBuilder.AddForeignKey(
                name: "FK_Profiles_Goals_GoalId",
                table: "Profiles",
                column: "GoalId",
                principalTable: "Goals",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Profiles_Programs_ProgramId",
                table: "Profiles",
                column: "ProgramId",
                principalTable: "Programs",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Profiles_Set_SetId",
                table: "Profiles",
                column: "SetId",
                principalTable: "Set",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_Profiles_Workouts_WorkoutId",
                table: "Profiles",
                column: "WorkoutId",
                principalTable: "Workouts",
                principalColumn: "Id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Profiles_Goals_GoalId",
                table: "Profiles");

            migrationBuilder.DropForeignKey(
                name: "FK_Profiles_Programs_ProgramId",
                table: "Profiles");

            migrationBuilder.DropForeignKey(
                name: "FK_Profiles_Set_SetId",
                table: "Profiles");

            migrationBuilder.DropForeignKey(
                name: "FK_Profiles_Workouts_WorkoutId",
                table: "Profiles");

            migrationBuilder.DropTable(
                name: "Set");

            migrationBuilder.DropIndex(
                name: "IX_Profiles_GoalId",
                table: "Profiles");

            migrationBuilder.DropIndex(
                name: "IX_Profiles_ProgramId",
                table: "Profiles");

            migrationBuilder.DropIndex(
                name: "IX_Profiles_SetId",
                table: "Profiles");

            migrationBuilder.DropIndex(
                name: "IX_Profiles_WorkoutId",
                table: "Profiles");

            migrationBuilder.DropColumn(
                name: "GoalId",
                table: "Profiles");

            migrationBuilder.DropColumn(
                name: "ProgramId",
                table: "Profiles");

            migrationBuilder.DropColumn(
                name: "SetId",
                table: "Profiles");

            migrationBuilder.DropColumn(
                name: "WorkoutId",
                table: "Profiles");
        }
    }
}
