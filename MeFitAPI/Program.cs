using System.Configuration;
using System.Reflection;
using System.Text.Json.Serialization;
using MeFitAPI.Data;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json;

var MyAllowSpecificOrigins = "_myAllowSpecificOrigins";

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

//builder.Services.AddControllers();
builder.Services.AddControllers().AddJsonOptions(x =>
                x.JsonSerializerOptions.ReferenceHandler = ReferenceHandler.IgnoreCycles);
builder.Services.AddAutoMapper(typeof(Program));
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddDbContext<MeFitDbContext>(options =>
    options.UseSqlServer(builder.Configuration["ConnectionStringSecrets:DefaultConnection"]));
//builder.Services.AddDbContext<MeFitDbContext>(options =>
 //   options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));

// Swagger docs 
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("v1", new OpenApiInfo
    {
        Title = "MeFit API",
        Version = "v1",
        Description = "An ASP.NET core Web API for the MeFit application.",
        Contact = new OpenApiContact
        {
            Name = "MeFit contact",
            Url = new Uri("https://gitlab.com/DrAvokad/mefit-backend/"),
        },
        License = new OpenApiLicense
        {
            Name = "Use under MIT",
            Url = new Uri("https://opensource.org/licenses/MIT"),
        }
    });
    // Set the comments path for the Swagger JSON and UI.
    var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
    var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
    c.IncludeXmlComments(xmlPath);
});

builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(builder =>
        {
            builder.SetIsOriginAllowed(origin => new Uri(origin).Host == "localhost")
            .AllowAnyHeader()
            .AllowAnyMethod();
        });

    // If cors doesnt work with deployed app (it should work)
    //options.AddDefaultPolicy(builder =>
    //{
    //    builder.SetIsOriginAllowed(origin => new Uri(origin).Host == "heroku")
    //    .AllowAnyHeader()
    //    .AllowAnyMethod();
    //});
    //options.AddPolicy(MyAllowSpecificOrigins,
    //    builder =>
    //    {
    //        builder.WithOrigins("https://mefit-noroff-swe.herokuapp.com/")
    //        .AllowAnyHeader()
    //        .AllowAnyMethod();
    //    });
});


builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddJwtBearer(options =>
    {
        options.TokenValidationParameters = new TokenValidationParameters
        {
        // Requires token from keycloak instance - location stored in secret manager
        IssuerSigningKeyResolver = (token, securityToken, kid, parameters) =>
            {
                var client = new HttpClient();
                var keyuri = builder.Configuration["TokenSecrets:KeyURI"];
                // Retrieves the keys from keycloak instance to verify token
                var response = client.GetAsync(keyuri).Result;
                var responseString = response.Content.ReadAsStringAsync().Result;
                var keys = JsonConvert.DeserializeObject<JsonWebKeySet>(responseString);
                return keys.Keys;
            },
            ValidIssuers = new List<string>
            {
                builder.Configuration["TokenSecrets:IssureURI"]
            },
            ValidAudience = "account",
            
        };
    });

// Set authorization roles here. Parameters in RequireRole is specified in the keycloak instance
builder.Services.AddAuthorization(options =>
{
    options.AddPolicy("Administrator", policy => policy.RequireRole("role", "[Administrator]"));
    options.AddPolicy("Contributor", policy => policy.RequireRole("role", "[Contributor, Administrator]"));
    options.AddPolicy("User", policy => policy.RequireRole("role", "[Administrator, Contributor, User]"));
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors();

app.UseHttpsRedirection();

app.UseAuthentication();

app.UseAuthorization();

app.MapControllers();

app.Run();
