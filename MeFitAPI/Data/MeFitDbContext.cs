﻿using System.Diagnostics.CodeAnalysis;
using MeFitAPI.Models;
using MeFitAPI.Models.Domain;
using Microsoft.EntityFrameworkCore;

namespace MeFitAPI.Data
{
    public class MeFitDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<ProfileModel> Profiles { get; set; }
        public DbSet<GymProgram> Programs { get; set; }
        public DbSet<Goal> Goals { get; set; }
        public DbSet<Workout> Workouts { get; set; }
        public DbSet<Exercise> Exercises { get; set; }
        public DbSet<Set> Sets { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Category> Categories { get; set; }

        public MeFitDbContext([NotNullAttribute] DbContextOptions options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GymProgram>().HasData(SeedHelper.GetProgramSeeds());
            modelBuilder.Entity<Workout>().HasData(SeedHelper.GetWorkoutSeeds());
            modelBuilder.Entity<Exercise>().HasData(SeedHelper.GetExerciseSeeds());
            modelBuilder.Entity<Goal>().HasData(SeedHelper.GetGoalSeeds());
            modelBuilder.Entity<Category>().HasData(SeedHelper.GetCategorySeeds());
        }
    }
}
