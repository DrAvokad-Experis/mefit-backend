using MeFitAPI.Models.Domain;

namespace MeFitAPI.Data
{
    public static class SeedHelper
    {
        public static ICollection<GymProgram> GetProgramSeeds()
        {
            return new List<GymProgram>()
            {
                new GymProgram(){ Id = 1, Name = "Zercher Positive", Category = "FULL"},
                new GymProgram(){ Id = 2, Name = "Booba Blowup", Category = "Body-building - Intermediate" },
                new GymProgram(){ Id = 3, Name = "Lovely Legs", Category = "Body-building - Beginner"},
                new GymProgram(){ Id = 4, Name = "Very clean", Category = "Weightlifting - Advanced"},
                new GymProgram(){ Id = 5, Name = "Dom's Gauntlet", Category = "Broscience"},
                new GymProgram(){ Id = 6, Name = "Extreme Crossfit", Category = "Stupidity"},
                new GymProgram(){ Id= 7, Name = "BDSM (Bench, Deadlift, Squat, Military-Press", Category = "Powerlifting - Beginner"}
            };
        }

        public static ICollection<Workout> GetWorkoutSeeds()
        {
            return new List<Workout>()
            {
                new Workout(){ Id = 1, Name = "Back/Biceps", Type = "Hypertrophy"},
                new Workout(){ Id = 2, Name = "Treadmill HIIT", Type = "Cardiovascular"},
                new Workout(){ Id = 3, Name = "Squat 1RM", Type = "Strength Training"},
                new Workout(){ Id = 4, Name = "Kyriakos Classic", Type = "Strength Training"},
                new Workout(){ Id = 5, Name = "Zercher Suicide", Type = "Strength Training"},
                new Workout(){ Id = 6, Name = "Bench-impress", Type = "Strength Training"},
                new Workout(){ Id = 7, Name = "Dumbbell Pressout", Type = "Hypertrophy"},
                new Workout(){ Id = 8, Name = "Chest vs Machine", Type = "Hypertrophy"},
                new Workout(){ Id = 9, Name = "Partial Arts", Type = "Technique"},
                new Workout(){ Id = 10, Name = "Party in the Quad", Type = "Hypertrophy"},
                new Workout(){ Id = 11, Name = "Booty Builder", Type = "Hypertrophy"},
                new Workout(){ Id = 12, Name = "Squat Sesh", Type = "Strength Training"},
                new Workout(){ Id = 13, Name = "Mr.Clean", Type = "Strength Training"},
                new Workout(){ Id = 14, Name = "Squat-rack Alpha", Type = "Technique"},
                new Workout(){ Id = 15, Name = "Own the Bench", Type = "Technique"},
                new Workout(){ Id = 16, Name = "Curls for Jesus", Type = "Hypertrophy"},
                new Workout(){ Id = 17, Name = "Barbell Mastery", Type = "Technique"},
                new Workout(){ Id = 18, Name = "Crazy Calisthenics", Type = "Technique"},
                new Workout(){ Id = 19, Name = "Unclean Cleans", Type = "Technique"},
                new Workout(){ Id = 20, Name = "Non-lethal Deadlifts", Type = "Strength Training"}
            };
        }

        public static ICollection<Exercise> GetExerciseSeeds()
        {
            return new List<Exercise>()
            {
                new Exercise(){ Id = 1, Name = "Bench Press", Description = "Lie down on a bench and press a barbell upwards from your chest.", TargetMuscleGroup = "Chest", Img = "benchpress.jpg", VideoLink = "Https://youtube.com/benchpress"},
                new Exercise(){ Id = 2, Name = "Deadlift", Description = "Stand up and pull a weighted barbell upwards.", TargetMuscleGroup = "Hamstrings", Img = "Deadlift.jpg"},
                new Exercise(){ Id = 3, Name = "Squat", Description = "Put a barbell on your scapulas and bend the knee to the king", TargetMuscleGroup = "Glutes"},
                new Exercise(){ Id = 4, Name = "Zercher Shrugs", Description = "Grip a barbell with your elbows with as much weight as you can possibly hold then shrug your shoulders. Grunt as loudly as possible during the exercise and finish off by screaming your lungs out", TargetMuscleGroup = "Trapezius", Img = "https://upload.wikimedia.org/wikipedia/commons/3/3a/Grizzly_Denali_Cop.jpg", VideoLink = "https://youtu.be/1LsIQr_4iSY"},
                new Exercise(){ Id = 5, Name = "Standing Forearm Curls", Description = "Hold a barbell over the end of your spine, resting your hands on the top of your glutes, then curl your hands upwards. Load barbell with as much weight as possible", TargetMuscleGroup = "Forearms", Img = "https://upload.wikimedia.org/wikipedia/commons/5/59/Flexor-carpi-radialis.png", VideoLink = "https://youtu.be/zcge1HFwpfw"},
                new Exercise(){ Id = 6, Name = "Zercher Squat", Description = "Load a barbell with adequate weight and hold it with your elbows. Then proceed to squat down until your knees are at a 90 degree angle, then press yourself back up", TargetMuscleGroup = "Quardriceps", Img = "https://upload.wikimedia.org/wikipedia/commons/1/16/Zecher-squats-1.gif", VideoLink = "https://youtu.be/SQTMbyFkcRI"},
                new Exercise(){ Id = 7, Name = "Zercher Deadlift", Description = "Load a barbell with adequate weight and pick it up from the ground with your elbows. Lift the barbell to a standing position, then lower the barbell back to the ground", TargetMuscleGroup = "Glutes", Img = "Zercher.png", VideoLink = "https://youtu.be/93Ef0C8mg5U"},
                new Exercise(){ Id = 8, Name = "Military Press", Description = "From a standing position press a barbell from your chest to above your head", TargetMuscleGroup = "Deltoids", Img = "https://upload.wikimedia.org/wikipedia/commons/5/5f/Lasha_Talakhadze_Rio_2016.jpg", VideoLink = "https://youtu.be/ryi5s0a9ZJQ"},
                new Exercise(){ Id = 9, Name = "Incline Benchpress", Description = "Lie down on a bench with the backrest angled up at about a 35 degrees and press a barbell upwards from your chest.", TargetMuscleGroup = "Pectorals", Img = "inclinepress.jpg", VideoLink = "https://youtu.be/67zS5weDiXQ"},
                new Exercise(){ Id = 10, Name = "Decline Benchpress", Description = "Lie down on a flat bench with the seat elevated higher thatn the backrest and press a barbell upwards from your chest.", TargetMuscleGroup = "Pectorals", Img = "declinepress.png", VideoLink = "https://youtu.be/QS2qQZsxBe0"},
                new Exercise(){ Id = 11, Name = "Dumbbell Press", Description = "Lie down on a flat bench and press a dumbbell in each hand upwards from your chest.", TargetMuscleGroup = "Pectorals", Img = "https://upload.wikimedia.org/wikipedia/commons/2/2e/Dumbbell-bench-press-1.png", VideoLink = "https://youtu.be/jRUC6IVav30"},
                new Exercise(){ Id = 12, Name = "Incline Dumbbell Press", Description = "Lie down on a bench with the backrest angled up at about a 35 degrees and press a dumbbell in each hand upwards from your chest", TargetMuscleGroup="Pectorals", Img="https://upload.wikimedia.org/wikipedia/commons/3/3d/Incline-press-2.png", VideoLink="https://youtu.be/wpBTgVfPq28"},
                new Exercise(){ Id = 13, Name = "Overhead Dumbell Press", Description = "From a standing position,  press a dumbbell up over your head", TargetMuscleGroup = "Deltoids", Img = "https://upload.wikimedia.org/wikipedia/commons/f/f2/One-arm-shoulder-press-1.png", VideoLink = "https://youtu.be/6pSRdyUTsPs"},
                new Exercise(){ Id = 14, Name = "Machine Chest Press", Description = "Grip the handles of the machine and press them forwards", TargetMuscleGroup = "Pectorals", Img = "https://upload.wikimedia.org/wikipedia/commons/4/48/Incline-bench-press-2-1.png", VideoLink = "https://youtu.be/2y6ntGVg4dw"},
                new Exercise(){ Id = 15, Name = "Zercher Pull-up", Description = "Grip a fastened pull-up bar with your elbows and somehow manage to pull yourself from the ground", TargetMuscleGroup = "Unknown", Img = "madness.jpg", VideoLink="zercher.mp4"},
                new Exercise(){ Id = 16, Name = "Zercher Curls", Description = "Grip a barbell with the crooks of your elbows and curl it", TargetMuscleGroup = "Biceps", Img = "zerchercurl.svg", VideoLink = "advancedcurling.mp4"},
                new Exercise(){ Id = 17, Name = "Hacksquat", Description = "The hack squat involves standing on the plate of a machine, leaning back onto its the pads at an angle, with the weight placed on top of you by positioning yourself under the shoulder pads. The weight is then pushed in the concentric phase of the squat", TargetMuscleGroup = "Glutes", Img = "https://upload.wikimedia.org/wikipedia/commons/9/98/Hack-squats-2.gif", VideoLink = "https://youtu.be/dWtaVd_XmX4"},
                new Exercise(){ Id = 18, Name = "Front Squat", Description = "Put a barbell on top of the front of your shoulders and squat down to a 90 degree angle and then stand back up.", TargetMuscleGroup = "Quadriceps", Img = "https://upload.wikimedia.org/wikipedia/commons/0/02/Front-squat-2-857x1024.png", VideoLink = "https://youtu.be/iytyDf-yJZs"},
                new Exercise(){ Id = 19, Name = "Leg Curl", Description = "Put weight on the back of your ankle with the help of a machine and curl your leg upwards", TargetMuscleGroup = "Hamstrings", Img = "https://upload.wikimedia.org/wikipedia/commons/f/fe/Standing-leg-curl-1.png", VideoLink = "https://youtu.be/d4PQv5D06_Y"},
                new Exercise(){ Id = 20, Name = "Clean & Jerk", Description = " During the clean, the lifter moves the barbell from the floor to a racked position across the deltoids, without resting fully on the clavicles. During the jerk, the lifter raises the barbell to a stationary position above the head, finishing with straight arms and legs, and the feet in the same plane as the torso and barbell.", TargetMuscleGroup = "Full Body", Img = "https://upload.wikimedia.org/wikipedia/commons/a/af/EVD-pesas-056.jpg", VideoLink = "https://youtu.be/IIHZUR67JsY"},
                new Exercise(){ Id = 21, Name = "Curls in the Squat Rack", Description = "Even if you could curl a barbell any other place in the gym, purposefully curl in the squat rack to show your dominance over all other gym-goers and your disdain of leg day", TargetMuscleGroup = "Ego", Img = "bro.png", VideoLink = "brosciencelife.mp4"},
                new Exercise(){ Id = 22, Name = "Flex in the Squat Rack", Description = "Occupy the squat rack by flexing, boosting your ego and blocking any attempts at leg workouts", TargetMuscleGroup = "Ego", Img = "dom.png", VideoLink = "https://youtube.com/brosciencelife"},
                new Exercise(){ Id = 23, Name = "Curl At the Bench", Description = "After you have proven to everyone how much you can bench proceed to get a nice arm-pump by curling the barbell", TargetMuscleGroup = "Ego", Img = "mazetti.jpg", VideoLink = "alpha.mp4"},
                new Exercise(){ Id = 24, Name = "Bicep Curls", Description = "Grip a dumbbell and curl it bro", TargetMuscleGroup = "Biceps", Img = "https://upload.wikimedia.org/wikipedia/commons/c/c6/Two-arm-preacher-curl-2.gif", VideoLink = "https://youtube.com/shorts/q99mHEoIw-Q?feature=share"},
                new Exercise(){ Id = 25, Name = "Hammer Curls", Description = "This curl is performed simultaneously with dumbbells but without wrist supination. Throughout each rep, the wrists remain neutral, like a carpenter hammering a nail.", Img = "hammercurl.png", VideoLink = "https://youtu.be/WGTCVgCqLqM"},
                new Exercise(){ Id = 26, Name = "Crossfit Pull-up", Description = "Hoist yourself violently up a pull-up bar swinging back and forth wildly. Exit the movement by hurling yourself face-first into the ground", TargetMuscleGroup = "Pain Receptors", Img = "https://upload.wikimedia.org/wikipedia/commons/c/ca/Clown.svg", VideoLink = "https://youtube.com/shorts/WfhQNv0otBQ?feature=share"},
                new Exercise(){ Id = 27, Name = "Crossfit Clean & Jerk", Description = "Just like a normal clean and jerk but try to injure yourself in the process", TargetMuscleGroup = "Face", Img = "https://upload.wikimedia.org/wikipedia/commons/c/ca/Clown.svg", VideoLink = "https://youtube.com/shorts/Yt0NifZskeU?feature=share"}
            };
        }

        public static ICollection<Goal> GetGoalSeeds()
        {
            return new List<Goal>()
            {
                new Goal() { Id = 1, EndDate = new DateTime(2022, 4, 20), ProgramId = 2, Achieved = false },
                new Goal() { Id = 2, EndDate = new DateTime(2022, 5, 20), ProgramId = 1, Achieved = false },
                new Goal() { Id = 3, EndDate = new DateTime(2022, 3, 14), ProgramId = 3, Achieved = false }
            };
        }

        public static ICollection<Category> GetCategorySeeds()
        {
            return new List<Category>()
            {
                new Category() { Id = 1, Name = "FULL"},
                new Category() { Id = 2, Name = "Body-building - Intermediate"},
                new Category() { Id = 3, Name = "Body-building - Beginner"},
                new Category() { Id = 4, Name = "Weightlifting - Advanced"},
                new Category() { Id = 5, Name = "Broscience"},
                new Category() { Id = 6, Name = "Stupidity"},
                new Category() { Id = 7, Name = "Powerlifting - Beginner"}
            };
        }
    }
}
