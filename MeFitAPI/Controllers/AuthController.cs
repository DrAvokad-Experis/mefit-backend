﻿using AutoMapper;
using MeFitAPI.Data;
using MeFitAPI.Models.DTOs.Profile;
using MeFitAPI.Models.DTOs.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MeFitAPI.Controllers
{
    /// <summary>
    /// This class is only for the purpose of testing authorization with roles specified in Program.cs
    /// Currently available roles are: User, Contributor and Administrator
    /// </summary>
    [EnableCors(PolicyName = "MyAllowSpecificOrigins")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        public AuthController(MeFitDbContext context, IMapper mapper)
        { 
        }

        [EnableCors]
        [HttpGet("noauth")]
        public string TestNoAuth()
        {
            return "successful non-authorized request";
        }

        [EnableCors]
        [HttpGet("userauth")]
        [Authorize(Roles = "User")]
        public string TestUserAuth()
        {
            return "successfully authorized as a user";
        }

        [EnableCors]
        [HttpGet("contributorauth")]
        [Authorize(Roles = "Contributor")]
        public string TestContributorAuth()
        {
            return "successfully authorized as a contributor";
        }
        [EnableCors]
        [HttpGet("administratorauth")]
        [Authorize(Roles = "Administrator")]
        public string TestAdministratorAuth()
        {
            return "successfully authorized as an administrator";
        }
    }
}
