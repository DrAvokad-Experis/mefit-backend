﻿using AutoMapper;
using MeFitAPI.Data;
using MeFitAPI.Models.Domain;
using MeFitAPI.Models.DTOs.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MeFitAPI.Controllers
{
    [Authorize]
    [EnableCors]
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController: ControllerBase
    {
        private readonly MeFitDbContext _context;
        private readonly IMapper _mapper;

        public UsersController(MeFitDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Fetch all users in the database.
        /// </summary>
        /// <returns>List of users</returns>
        /// <response code="200">Successfully fetched the users</response>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserReadDTO>>> GetUsers()
        {
            var users = await _context.Users
                .ToListAsync();

            return Ok(_mapper.Map<List<UserReadDTO>>(users));
        }

        /// <summary>
        ///  Gets specific user by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A single user</returns>
        /// <response code="200">Successfully fetched user.</response>
        /// <response code="404">User could not be found.</response>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<UserReadDTO>> GetUser(int id)
        {
            var user = _mapper.Map<UserReadDTO>(await _context.Users
                .FirstOrDefaultAsync(u => u.Id == id));

            if(user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }

        /// <summary>
        /// Adds a user to the database.
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        /// <response code="201">Successfully created a user</response>
        /// <response code="400">Parameter was null</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<UserCreateDTO>> PostUser(UserCreateDTO user)
        {
            if(user == null)
            {
                return BadRequest();
            }

            var domainUser = _mapper.Map<User>(user);

            await _context.Users.AddAsync(domainUser);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetUser", new {id = domainUser.Id}, domainUser);
        }

        /// <summary>
        /// Deletes a user by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="204">Successfully deleted profile.</response>
        /// <response code="404">User could not be found.</response>
        [Authorize(Roles = "Administrator")]
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> DeleteUser(int id)
        {
            var user = await _context.Users.FindAsync(id);

            if (user == null)
            {
                return NotFound();
            }

            _context.Remove(user);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
