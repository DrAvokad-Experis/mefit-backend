﻿using AutoMapper;
using MeFitAPI.Data;
using MeFitAPI.Models.Domain;
using MeFitAPI.Models.DTOs;
using MeFitAPI.Models.DTOs.GymProgram;
using MeFitAPI.Models.DTOs.Workout;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MeFitAPI.Controllers
{
    [EnableCors]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class GymProgramController : ControllerBase
    {
        private readonly MeFitDbContext _context;
        private readonly IMapper _mapper;

        public GymProgramController(MeFitDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        #region CRUD
        /// <summary>
        /// Fetch all programs
        /// </summary>
        /// <returns></returns>
        /// <response code="200">Returns all workouts</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<GymProgramReadDTO>>> GetPrograms()
        {
            var programs = _mapper.Map<List<GymProgramReadDTO>>(await _context.Programs
               .Include(p => p.Workouts)
               .ToListAsync());

            return Ok(programs);
        }

        /// <summary>
        /// Fetch specific program by Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A Program object</returns>
        /// <response code="200">Returns all workouts</response>
        /// <response code="404">Program not found</response>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<GymProgramReadDTO>> GetProgram(int id)
        {
            var program = await _context.Programs
                .Include(p => p.Workouts)
                .FirstOrDefaultAsync(p => p.Id == id);

            if (program == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<GymProgramReadDTO>(program));
        }

        /// <summary>
        /// Posts a new Gym Program to the database.
        /// </summary>
        /// <param name="dtoProgram">DTO of the program to post to the database</param>
        /// <returns>DTO of the program that was posted to the database</returns>
        /// <response code="200">The Gym Program was successfully added to the database</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<GymProgramReadDTO>> PostProgram(GymProgramCreateDTO dtoProgram)
        {
            GymProgram domainProgram = _mapper.Map<GymProgram>(dtoProgram);
            _context.Add(domainProgram);
            await _context.SaveChangesAsync();

            var readProgram = CreatedAtAction("GetProgram", new { id = domainProgram.Id }, _mapper.Map<GymProgramReadDTO>(domainProgram));
            return Ok(readProgram);
        }

        /// <summary>
        /// Removes the specified Gym Program from the database if it exists
        /// </summary>
        /// <param name="id">Id of the Program to remove</param>
        /// <returns></returns>
        /// <response code="204">The Gym Program was successfully removed from the database</response>
        /// <response code="404">The specified Gym Program does not exist in the database</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteProgram(int id)
        {
            try
            {
                var program = await _context.Programs.FindAsync(id);

                _context.Programs.Remove(program);
                await _context.SaveChangesAsync();

                return NoContent();

            }
            catch (Exception)
            {
                NotFound();
            }

            return NotFound();
        }


        #endregion

        /// <summary>
        /// Takes the id of a Gym Program and an array of Workout-id and assigns the corresponding workouts to the specified Gym Program.
        /// </summary>
        /// <param name="id">Id of the Gym Program</param>
        /// <param name="workoutIds">An array of workout-ids to assign to the Gym Program</param>
        /// <returns></returns>
        /// <response code="204">Workouts where successfully assigned to the Gym Program</response>
        /// <response code="404">The specified program or workouts could not be found in the database</response>
        [HttpPost("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = "Contributor")]
        public async Task<ActionResult> AssignWorkoutsToGymProgram(int id, [FromBody] int[] workoutIds)
        {
            var gymProgram = await _context.Programs
                .Include(p => p.Workouts)
                .Where(p => p.Id == id)
                .FirstAsync();

            foreach (var workoutId in workoutIds)
            {
                var workout = await _context.Workouts.FindAsync(workoutId);

                if(gymProgram == null || workout == null)
                {
                    return NotFound();
                }

                gymProgram.Workouts.Add(workout);
            }

            await _context.SaveChangesAsync();

            return NoContent();
        }

        // -- WIP
        /// <summary>
        /// Fetch workouts in a specific program.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A list of workouts</returns>
        /// <response code="200">Returns all workouts in the program</response>
        /// <response code="404">Program not found</response>
        [HttpGet("workouts/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ICollection<WorkoutReadDTO>>> GetAllWorkoutsInProgram(int id)
        {
            GymProgram program = await _context.Programs
                 .Include(p => p.Workouts)
                 .Where(p => p.Id == id)
                 .FirstAsync();

            if (program == null)
            {
                return NotFound();
            }

            var workouts = program.Workouts;

            return Ok(_mapper.Map<List<WorkoutReadDTO>>(workouts));
        }

        [HttpGet("category/{categoryName}")]
        public async Task<ActionResult<ICollection<GymProgramReadDTO>>> GetProgramsByCategory(string categoryName)
        {
            var programs = await _context.Programs
                .Include(p => p.Workouts)
                .ToListAsync();
            List<GymProgram> result = new();

            foreach(var program in programs)
            {
                if(program.Category != null && program.Category.ToLower() == categoryName.ToLower())
                {
                    result.Add(program);
                }
            }

            return _mapper.Map<List<GymProgramReadDTO>>(result);
        }
    }
}
