﻿using AutoMapper;
using MeFitAPI.Data;
using MeFitAPI.Models.Domain;
using MeFitAPI.Models.DTOs.Exercise;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MeFitAPI.Controllers
{
    [EnableCors]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ExerciseController : ControllerBase
    {
        private readonly MeFitDbContext _context;
        private readonly IMapper _mapper;

        public ExerciseController(MeFitDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        /// <summary>
        /// Fetch all exercises from database.
        /// </summary>
        /// <returns>A list of exercises</returns>
        /// <response code="200">Returns all workouts</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<ExerciseReadDTO>>> GetAllExercises()
        {
            var exercises = await _context.Exercises
                .Include(e => e.Workouts)
                .Include(e => e.Sets)
                .ToListAsync();

            return Ok(_mapper.Map<List<ExerciseReadDTO>>(exercises));
        }

        /// <summary>
        /// Get exercise by Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>An exercise object</returns>
        /// <response code="200">Returns all workouts</response>
        /// <response code="404">Exercise was not found</response>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ExerciseReadDTO>> GetExercises(int id)
        {
            var exercise = _mapper.Map<ExerciseReadDTO>(await _context.Exercises
                .Include(e => e.Workouts)
                .Where(e => e.Id == id)
                .Include(e => e.Sets)
                .Where(e => e.Id == id)
                .FirstOrDefaultAsync());

            if (exercise == null)
            {
                return NotFound();
            }

            return Ok(exercise);
        }

        /// <summary>
        /// Posts a new exercise to the database.
        /// </summary>
        /// <param name="dtoExercise">DTO of the Exercise to post to the database</param>
        /// <returns>DTO of the Exercise posted to the database</returns>
        /// <response code="200">The Exercise was successfully added to the database</response>
        [HttpPost]
        [Authorize(Roles = "Contributor")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<ExerciseReadDTO>> PostExercise(ExerciseCreateDTO dtoExercise)
        {
            Exercise domainExercise = _mapper.Map<Exercise>(dtoExercise);
            _context.Add(domainExercise);
            await _context.SaveChangesAsync();

            var readExercise = CreatedAtAction("GetExercise", new { id = domainExercise.Id }, _mapper.Map<ExerciseReadDTO>(domainExercise));
            return Ok(readExercise);
        }

        /// <summary>
        /// Removes the specified Exercise from the database it it exists
        /// </summary>
        /// <param name="id">Id of the Exercise to remove</param>
        /// <returns></returns>
        /// <response code="204">The Exercise was successfully removed from the database</response>
        /// <response code="404">The specified Exercise does not exist in the database</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = "Contributor")]
        public async Task<ActionResult> DeleteExercise(int id)
        {
            try
            {
                var exercise = await _context.FindAsync<Exercise>(id);

                _context.Remove(exercise);
                await _context.SaveChangesAsync();

                return NoContent();

            }
            catch (Exception)
            {
                NotFound();
            }

            return NotFound();
        }

    }
}
