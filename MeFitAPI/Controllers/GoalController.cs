﻿using AutoMapper;
using MeFitAPI.Data;
using MeFitAPI.Models.Domain;
using MeFitAPI.Models.DTOs.Goal;
using MeFitAPI.Models.DTOs.Workout;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MeFitAPI.Controllers
{
    [EnableCors]
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class GoalController : ControllerBase
    {
        private readonly MeFitDbContext _context;
        private readonly IMapper _mapper;

        public GoalController(MeFitDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        #region CRUD

        /// <summary>
        /// Fetch all goals from database.
        /// </summary>
        /// <returns>All goals</returns>
        /// <response code="200">Goals were returned</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<GoalReadDTO>>> GetGoals()
        {
            var goals = _mapper.Map<List<GoalReadDTO>>(await _context.Goals
                .Include(g => g.Workouts)
                .ToListAsync());

            return Ok(goals);
        }

        /// <summary>
        /// Fetch specific goal by Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A goal object</returns>
        /// <response code="200">Goal was returned</response>
        /// <response code="404">If goal was not found</response>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<GoalReadDTO>> GetGoal(int id)
        {
            var goal = await _context.Goals
                .Include(g => g.Workouts)
                .Where(g => g.Id == id)
                .FirstOrDefaultAsync();

            if (goal == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<GoalReadDTO>(goal));
        }

        /// <summary>
        /// Create a new goal and put in Db
        /// </summary>
        /// <param name="goal"></param>
        /// <returns>The created goal</returns>
        /// <response code="200">Goal was returned</response>
        /// <response code="201">If goal was successfully created</response>
        /// <response code="400">If param was null</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<GoalCreateDTO>> CreateGoal(GoalCreateDTO goal)
        {
            if (goal == null)
            {
                return BadRequest();
            }

            Goal domainGoal = _mapper.Map<Goal>(goal);
            _context.Goals.Add(domainGoal);
            await _context.SaveChangesAsync();

            var readGoal = CreatedAtAction("GetGoal", new { id = domainGoal.Id }, _mapper.Map<GoalReadDTO>(domainGoal));
            return Ok(readGoal);
        }
        #endregion

        /// <summary>
        /// Get all the workouts in a goal
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List of workouts</returns>
        [HttpGet("workouts/{id}")]
        public async Task<ActionResult<List<WorkoutReadDTO>>> GetWorkoutsInGoal(int id)
        {
            var goal = await _context.Goals
                .Include(g => g.Workouts)
                .Where(g => g.Id == id)
                .FirstOrDefaultAsync();

            if (goal == null) return NotFound();

            var workouts = _mapper.Map<List<WorkoutReadDTO>>(goal.Workouts);

            return Ok(workouts);
        }

        /// <summary>
        /// Takes the id of a Goal and the id of a Gym Program and assigns the corresponding Program to the specified Goal. 
        /// </summary>
        /// <param name="programId">Id of the Gym Program to assign to the Goal</param>
        /// <param name="id">Id of the Goal</param>
        /// <returns></returns>
        /// <response code="204">Gym Program was successfully assigned to the Goal</response>
        /// <response code="404">The specified Gym Program or Goal could not be found in the database</response>
        [HttpPost("program/{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> AssignProgramToGoal([FromBody] int programId, int id)
        {
            Goal goal = await _context.Goals
                .Include(g => g.Program)
                .Where(g => g.Id == id)
                .FirstAsync();

            var program = await _context.Programs.FindAsync(programId);

            if (program == null || goal == null)
            {
                return NotFound();
            }

            goal.Program = program;

            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Takes the id of a Goal and the id of a Workout and assigns the corresponding Workout to the specified Goal.
        /// </summary>
        /// <param name="workoutId">Id of the Workout to assign to the Goal</param>
        /// <param name="id">Id of the Goal</param>
        /// <returns></returns>
        /// <response code="204">Workout was successfully assigned to the Goal</response>
        /// <response code="404">The specified Workout or Goal could not be found in the database</response>
        [HttpPost("workout/{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> AssignWorkoutToGoal([FromBody] int workoutId, int id)
        {
            Goal goal = await _context.Goals
                .Include(g => g.Program)
                .Where(g => g.Id == id)
                .FirstAsync();

            var workout = await _context.Workouts.FindAsync(workoutId);

            if (workout == null || goal == null)
            {
                return NotFound();
            }

            goal.Workouts.Add(workout);

            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Assign multiple workouts to a goal
        /// </summary>
        /// <param name="workoutIds"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("workouts/{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> AssignWorkoutsToGoal([FromBody] int[] workoutIds, int id)
        {
            var goal = await _context.Goals
                .Include(p => p.Workouts)
                .Where(p => p.Id == id)
                .FirstAsync();

            if (goal == null) return NotFound();

            foreach (var wId in workoutIds)
            {
                var workout = await _context.Workouts.FindAsync(wId);

                if (workout != null)
                    goal.Workouts.Add(workout);
            }

            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
