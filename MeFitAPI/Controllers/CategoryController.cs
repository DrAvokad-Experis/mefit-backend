﻿using AutoMapper;
using MeFitAPI.Data;
using MeFitAPI.Models.Domain;
using MeFitAPI.Models.DTOs.Category;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MeFitAPI.Controllers
{
    [Authorize]
    [EnableCors]
    [ApiController]
    [Route("api/[controller]")]
    public class CategoryController : ControllerBase
    {
        private readonly MeFitDbContext _context;
        private readonly IMapper _mapper;

        public CategoryController(MeFitDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }


        /// <summary>
        /// Fetch all categories from the database
        /// </summary>
        /// <returns>List of categories</returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<CategoryReadDTO>>> GetCategories()
        {
            var categories = await _context.Categories
                .Include(c => c.Profiles)
                .ToListAsync();

            if (categories == null) return NotFound();

            return Ok(_mapper.Map<List<CategoryReadDTO>>(categories));

        }
    }
}
