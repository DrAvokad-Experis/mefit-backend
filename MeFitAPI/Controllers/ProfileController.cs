﻿using AutoMapper;
using MeFitAPI.Data;
using MeFitAPI.Models;
using MeFitAPI.Models.Domain;
using MeFitAPI.Models.DTOs.Category;
using MeFitAPI.Models.DTOs.Profile;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MeFitAPI.Controllers
{
    [EnableCors]
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class ProfileController : ControllerBase
    {
        private readonly MeFitDbContext _context;
        private readonly IMapper _mapper;

        public ProfileController(MeFitDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        #region CRUD
        /// <summary>
        /// Fetch all profiles from the db.
        /// </summary>  
        /// <returns>List of profiles</returns>
        /// <response code="200">Successfully fetched profiles</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<List<ProfileModel>>> GetProfiles()
        {
            var profiles = await _context.Profiles
                .Include(p => p.GymPrograms)
                .Include(p => p.Exercises)
                .Include(p => p.Workouts)
                .ToListAsync();

            return Ok(profiles);
        }

        /// <summary>
        /// Get profile by id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>A profile</returns>
        /// <response code="200">Returns profile</response>
        /// <response code="404">Profile was not found</response>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ProfileReadDTO>> GetProfile(int id)
        {
            var profile = await _context.Profiles
                .Include(p => p.GymPrograms)
                .Include(p => p.Exercises)
                .Include(p => p.Workouts)
                .Include(p => p.Categories)
                .FirstOrDefaultAsync(p => p.Id == id);

            if (profile == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<ProfileReadDTO>(profile));
        }

        /// <summary>
        /// Fetch a single profile by specifying userId.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>A single profile</returns>
        /// <response code="200">Successfully fetched the profile</response>
        /// <response code="404">Profile could not be found.</response>
        [HttpGet("user/{userId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ProfileReadDTO>> GetProfileByUserId(string userId)
        {
            var profile = await _context.Profiles
                .Include(p => p.GymPrograms)
                .Include(p => p.Exercises)
                .Include(p => p.Workouts)
                .FirstOrDefaultAsync(p => p.UserId == userId);

            if (profile == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<ProfileReadDTO>(profile));
        }

        // This function is the same as the get profile function, wip?
        [HttpGet("userContributions/{userId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ProfileModel>> GetUserContributionsByUserId(string userId)
        {
            var profile = await _context.Profiles
                .Include(p => p.GymPrograms)
                .Include(p => p.Exercises)
                .Include(p => p.Workouts)
                .FirstOrDefaultAsync(p => p.UserId == userId);

            if (profile == null)
            {
                return NotFound();
            }

            return Ok(profile);
        }

        /// <summary>
        /// Add new profile to database.
        /// </summary>
        /// <param name="profile"></param>
        /// <param name="userId"></param>
        /// <returns></returns>
        /// <response code="201">Successfully created profile</response>
        /// <response code="400">Parameter was null</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> CreateProfile(ProfileCreateDTO profile, string userId)
        {
            if (profile == null)
            {
                return BadRequest();
            }


            var profiles = await _context.Profiles.ToListAsync();
            int multipleProfiles = 0;
            profiles.ForEach(p =>
            {
                if (p.UserId == userId)
                {
                    multipleProfiles++;
                }
            });

            if (multipleProfiles > 0) return BadRequest();

            profile.UserId = userId;
            var domainProfile = _mapper.Map<ProfileModel>(profile);

            await _context.Profiles.AddAsync(domainProfile);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetProfile", new { id = domainProfile.Id }, _mapper.Map<ProfileReadDTO>(domainProfile));
        }

        /// <summary>
        /// Delete a profile by Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <response code="404">Profile to delete could not be found.</response>
        /// <response code="204">The specified Profile was deleted successfully.</response>
        [HttpDelete]
        [Authorize(Roles = "Administrator")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<ActionResult> DeleteProfile(int id)
        {
            var profile = await _context.Profiles.FindAsync(id);

            if (profile == null)
            {
                return NotFound();
            }

            try
            {
                _context.Profiles.Remove(profile);
                await _context.SaveChangesAsync();

                return NoContent();
            }
            catch (DbUpdateConcurrencyException)
            {
                throw;
            }
        }
        #endregion

        /// <summary>
        /// Takes the id of a Profile and the id of an Address and assigns the corresponding Address to the specified Profile.
        /// </summary>
        /// <param name="id">Id of the Profile</param>
        /// <param name="addressId">Id of the Address to assign to the Profile</param>
        /// <returns></returns>
        /// <response code="204">Address was successfully assigned to the Profile</response>
        /// <response code="404">The specified Profile or Address could not be found in the database</response>
        [HttpPost("address/{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> AssignAddressToProfile(int id, [FromBody] int addressId)
        {
            var profile = await _context.Profiles
                .Include(p => p.Address)
                .Where(p => p.Id == id)
                .FirstAsync();

            Address address = await _context.Addresses.FindAsync(addressId);

            if (address == null || profile == null)
            {
                return NotFound();
            }

            profile.Address = address;

            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Takes the id of a Profile and the id of a Goal and assigns the corresponding Goal to the specified Profile.
        /// </summary>
        /// <param name="id">Id of the Profile</param>
        /// <param name="goalId">Id of the Goal to assign to the Profile</param>
        /// <returns></returns>
        /// <response code="204">Goal was successfully assigned to the Profile</response>
        /// <response code="404">The specified Profile or Goal could not be found in the database</response>
        [HttpPost("goal/{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> AssignGoalToProfile(int id, [FromBody] int goalId)
        {
            var profile = await _context.Profiles
                .Include(p => p.Address)
                .Where(p => p.Id == id)
                .FirstAsync();

            Goal goal = await _context.Goals.FindAsync(goalId);

            if (goal == null || profile == null)
            {
                return NotFound();
            }

            profile.Goal = goal;

            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Get all categories from a specific profile
        /// </summary>
        /// <param name="profileId"></param>
        /// <returns>List of categories</returns>
        [EnableCors]
        [HttpGet("categories/{profileId}")]
        public async Task<ActionResult<ICollection<CategoryReadDTO>>> GetProfileCategories(int profileId)
        {
            var profile = await _context.Profiles
                .Include(p => p.Categories)
                .Where(p => p.Id == profileId)
                .FirstAsync();

            if (profile == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<List<CategoryReadDTO>>(profile.Categories));

        }

        /// <summary>
        /// Assign array of category Ids to the profile category list
        /// </summary>
        /// <param name="profileId"></param>
        /// <param name="categoryIds"></param>
        /// <returns></returns>
        [HttpPost("categories/{profileId}")]
        public async Task<ActionResult> AssignCategoriesToProfile(int profileId, [FromBody] int[] categoryIds)
        {
            var profile = await _context.Profiles
                .Include(p => p.Categories)
                .Where(p => p.Id == profileId)
                .FirstAsync();

            if (profile == null) return NotFound();

            profile.Categories.Clear();
            foreach (int id in categoryIds)
            {
                var category = _context.Categories.Find(id);

                if (category != null)
                    profile.Categories.Add(category);
            }

            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Assign a single category for a profile
        /// </summary>
        /// <param name="profileId"></param>
        /// <param name="categoryId"></param>
        /// <returns></returns>
        [HttpPost("category/{profileId}")]
        public async Task<ActionResult> AssignCategoryToProfile(int profileId, [FromBody] int categoryId)
        {
            var profile = await _context.Profiles
                .Include(p => p.Categories)
                .Where(p => p.Id == profileId)
                .FirstOrDefaultAsync();

            if (profile == null) return NotFound();

            var category = await _context.Categories.FindAsync(categoryId);
            if (category == null) return NotFound();

            profile.Categories.Add(category);

            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
