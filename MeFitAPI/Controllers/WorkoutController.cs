﻿using AutoMapper;
using MeFitAPI.Data;
using MeFitAPI.Models.Domain;
using MeFitAPI.Models.DTOs.Exercise;
using MeFitAPI.Models.DTOs.Workout;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MeFitAPI.Controllers
{
    [Authorize]
    [EnableCors]
    [Route("api/[controller]")]
    [ApiController]
    public class WorkoutController : ControllerBase
    {
        private readonly MeFitDbContext _context;
        private readonly IMapper _mapper;

        public WorkoutController(MeFitDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        #region CRUD
        /// <summary>
        /// Fetch all workouts from the database.
        /// </summary>
        /// <returns>List of workouts</returns>
        /// <response code="200">Returns all workouts</response>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<IEnumerable<WorkoutReadDTO>>> GetAllWorkouts()
        {
            var workouts = await _context.Workouts
                .Include(w => w.Programs)
                .Include(w => w.Exercises)
                .Include(w => w.Set)
                .ToListAsync();

            return Ok(_mapper.Map<List<WorkoutReadDTO>>(workouts));
        }

        /// <summary>
        /// Fetch specific workout by Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Workout object</returns>
        /// <response code="200">Returns workout</response>
        /// <response code="404">If no workout was found</response>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<WorkoutReadDTO>> GetWorkout(int id)
        {
            var workout = _mapper.Map<WorkoutReadDTO>(await _context.Workouts
                .Include(w => w.Programs)
                .Where(w => w.Id == id)
                .Include(w => w.Exercises)
                .Where(w => w.Id == id)
                .Include(w => w.Set)
                .Where(w => w.Id == id)
                .FirstOrDefaultAsync());

            if (workout == null)
            {
                return NotFound();
            }

            return Ok(workout);
        }

        /// <summary>
        /// Posts a new Workout to the database.
        /// </summary>
        /// <param name="dtoWorkout">DTO of the Workout to post to the database</param>
        /// <returns>DTO of the Workout posted to the database</returns>
        /// <response code="200">The Workout was successfully added to the database</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<ActionResult<WorkoutReadDTO>> PostWorkout(WorkoutCreateDTO dtoWorkout)
        {
            Workout domainWorkout = _mapper.Map<Workout>(dtoWorkout);
            _context.Add(domainWorkout);
            await _context.SaveChangesAsync();

            var readWorkout = CreatedAtAction("GetWorkout", new { id = domainWorkout.Id }, _mapper.Map<WorkoutReadDTO>(domainWorkout));
            return Ok(readWorkout);
        }

        /// <summary>
        /// Removes the specified Workout from the database if it exists
        /// </summary>
        /// <param name="id">Id of the Workout to remove</param>
        /// <returns></returns>
        /// <response code="204">The Workout was successfully removed from the database</response>
        /// <response code="404">The specified Workout does not exist in the database</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteWorkout(int id)
        {
            try
            {
                var workout = await _context.Workouts.FindAsync(id);


                _context.Workouts.Remove(workout);
                await _context.SaveChangesAsync();

                return NoContent();

            }
            catch (Exception)
            {
                NotFound();
            }

            return NotFound();
        }

        #endregion

        /// <summary>
        /// Takes the id of a Workout and the id of a Set and assigns the corresponding Set to the specified Workout.
        /// </summary>
        /// <param name="id">The id of the Workout</param>
        /// <param name="setId">The id of the set</param>
        /// <returns></returns>
        /// <response code="204">The Set was successfully assigned to the workout</response>
        /// <response code="404">The specified Workout or Set could not be found in the database</response>
        [HttpPost("exercises/{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> AssignSetToWorkout(int id, [FromBody] int setId)
        {
            var workout = await _context.Workouts
                .Include(w => w.Set)
                .Where(w => w.Id == id)
                .FirstAsync();

            var set = await _context.Sets.FindAsync(setId);

            if(set == null || workout == null)
            {
                return NotFound();
            }

            workout.Set = set;
            await _context.SaveChangesAsync();

            return NoContent();
        }

        /// <summary>
        /// Assign exercises by specifying an array of exercise ids to a workout id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="ExerciseIds"></param>
        /// <returns></returns>
        [HttpPost("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> AssignExercisesToWorkout(int id, [FromBody] int[] ExerciseIds)
        {
            var workout = await _context.Workouts
                .Include(w => w.Exercises)
                .Where(w => w.Id == id)
                .FirstAsync();

            if (workout == null)
            {
                return NotFound();
            }

            foreach (var exerciseId in ExerciseIds)
            {
                var exercise = await _context.Exercises.FindAsync(exerciseId);

                workout.Exercises.Add(exercise);
            }

            await _context.SaveChangesAsync();

            return NoContent();
        }
        /// <summary>
        /// Get list of exercises from program by Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns>List of exercises</returns>
        [HttpGet("exercises/{id}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<ICollection<ExerciseReadDTO>>> GetAllExercisesInWorkout(int id)
        {
            Workout workout = await _context.Workouts
                 .Include(w => w.Exercises)
                 .Where(w => w.Id == id)
                 .FirstAsync();

            if (workout == null)
            {
                return NotFound();
            }

            var exercises = workout.Exercises;
            if(exercises == null) return NoContent();

            return Ok(_mapper.Map<List<ExerciseReadDTO>>(exercises));
        }
    }
}
