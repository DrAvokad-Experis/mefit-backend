﻿using AutoMapper;
using MeFitAPI.Data;
using MeFitAPI.Models.Domain;
using MeFitAPI.Models.DTOs.Set;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace MeFitAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SetController : ControllerBase
    {
        private readonly MeFitDbContext _context;
        private readonly IMapper _mapper;

        public SetController(MeFitDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
         }

        [HttpGet("{id}")]
        public async Task<ActionResult<SetReadDTO>> GetSet(int id)
        {
            var set = _mapper.Map<SetReadDTO>(await _context.Sets
                .Include(s => s.Exercise)
                .Where(s => s.Id == id)
                .FirstOrDefaultAsync());

            if (set == null)
            {
                return NotFound();
            }

            return Ok(set);
        }

        /// <summary>
        /// Posts a new set of an exercise to the database
        /// </summary>
        /// <param name="dtoSet">The exercise-set to post to the database</param>
        /// <param name="exerciseid">The id of the exercise the set consists of</param>
        /// <returns></returns>
        /// <response code="200">Returns a read DTO of the created set</response>
        /// <response code="404">The exercise could not be found in the database</response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<SetReadDTO>> CreateSet(SetCreateDTO dtoSet, int exerciseid)
        {
            Set domainSet = _mapper.Map<Set>(dtoSet);
            var exercise = await _context.Exercises.FindAsync(exerciseid);

            if (exercise == null)
            {
                return NotFound();
            }

            domainSet.Exercise = exercise;
            
            _context.Add(domainSet);
            await _context.SaveChangesAsync();

            var readSet = CreatedAtAction("GetSet", new { id = domainSet.Id }, _mapper.Map<SetReadDTO>(domainSet));
            return Ok(readSet);          

        }

        /// <summary>
        /// Removes the specified Set from the database if it exists
        /// </summary>
        /// <param name="id">Id of the Set to remove</param>
        /// <returns></returns>
        /// <response code="204">The Set was successfully removed from the database</response>
        /// <response code="404">The specified Set does not exist in the database</response>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult> DeleteSet(int id)
        {
            try
            {
                var set = await _context.Sets.FindAsync(id);

                _context.Sets.Remove(set);
                await _context.SaveChangesAsync();

                return NoContent();

            }
            catch (Exception)
            {
                NotFound();
            }

            return NotFound();
        }
    }
}
