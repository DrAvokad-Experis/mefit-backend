﻿using AutoMapper;
using MeFitAPI.Data;
using MeFitAPI.Models;
using MeFitAPI.Models.DTOs.Address;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace MeFitAPI.Controllers
{
    [EnableCors]
    [ApiController]
    [Route("api/[controller]")]
    public class AddressController : ControllerBase
    {
        private readonly MeFitDbContext _context;
        private readonly IMapper _mapper;

        public AddressController(MeFitDbContext context, IMapper mapper)
        {
            this._context = context;
            this._mapper = mapper;
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<AddressReadDTO>> GetAddress(int id)
        {
            var address = await _context.Addresses.FindAsync(id);

            if (address == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<AddressReadDTO>(address));
        }

        [HttpPost]
        public async Task<ActionResult> CreateAddress(AddressCreateDTO address)
        {
            if (address == null)
            {
                return BadRequest();
            }
            Address domainAddress = _mapper.Map<Address>(address);

            await _context.AddAsync(domainAddress);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetAddress", new { id = domainAddress.Id }, _mapper.Map<AddressReadDTO>(domainAddress));
        }

    }
}
