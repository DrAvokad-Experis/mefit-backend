﻿using MeFitAPI.Models.Domain;
using MeFitAPI.Models.DTOs;
using MeFitAPI.Models.DTOs.GymProgram;

namespace MeFitAPI.Profiles
{
    public class ProgramProfile : AutoMapper.Profile
    {
        public ProgramProfile()
        {
            CreateMap<GymProgram, GymProgramReadDTO>()
                .ForMember(pdto => pdto.Workouts, opt => opt
                .MapFrom(p => p.Workouts.Select(p => p.Id).ToArray()))
                .ForMember(pdto => pdto.Profile, opt => opt
                .MapFrom(p => p.ProfileId))
                .ReverseMap();
            CreateMap<GymProgram, GymProgramCreateDTO>()
                .ForMember(pdto => pdto.Profile, opt => opt
                .MapFrom(p => p.ProfileId))
                .ReverseMap();
            CreateMap<GymProgramEditDTO, GymProgram>()
                .ReverseMap();
        }
    }
}