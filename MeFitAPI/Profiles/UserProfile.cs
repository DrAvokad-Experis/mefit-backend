﻿using MeFitAPI.Models.Domain;
using MeFitAPI.Models.DTOs.User;

namespace MeFitAPI.Profiles
{
    public class UserProfile : AutoMapper.Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserReadDTO>()
                .ReverseMap();
            CreateMap<UserCreateDTO, User>()
                .ReverseMap();
        }
    }
}
