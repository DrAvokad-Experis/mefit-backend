﻿using MeFitAPI.Models.Domain;
using MeFitAPI.Models.DTOs.Goal;

namespace MeFitAPI.Profiles
{
    public class GoalProfile : AutoMapper.Profile
    {
        public GoalProfile()
        {
            CreateMap<Goal, GoalReadDTO>()
                .ForMember(gdto => gdto.Workouts, opt => opt
                .MapFrom(g => g.Workouts.Select(g => g.Id).ToArray()))
                .ForMember(gdto => gdto.Program, opt => opt
                .MapFrom(g => g.ProgramId))
                .ReverseMap();
            CreateMap<GoalCreateDTO, Goal>()
                .ReverseMap();
        }
    }
}
