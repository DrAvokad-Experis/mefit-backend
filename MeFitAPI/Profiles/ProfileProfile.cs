using MeFitAPI.Models.Domain;
using MeFitAPI.Models.DTOs.Profile;

namespace MeFitAPI.Profiles
{
    public class ProfileProfile : AutoMapper.Profile
    {
        public ProfileProfile()
        {
            CreateMap<ProfileModel, ProfileReadDTO>()
                .ForMember(pdto => pdto.Programs, opt => opt
                .MapFrom(p => p.GymPrograms.Select(p => p.Id).ToArray()))
                .ForMember(pdto => pdto.Workouts, opt => opt
                .MapFrom(p => p.Workouts.Select(p => p.Id).ToArray()))
                .ForMember(pdto => pdto.Exercises, opt => opt
                .MapFrom(p => p.Exercises.Select(p => p.Id).ToArray()))
                .ForMember(pdto => pdto.Goal, opt => opt
                .MapFrom(p => p.Goal.Id))
                .ForMember(pdto => pdto.Set, opt => opt
                .MapFrom(p => p.Set.Id))
                .ForMember(pdto => pdto.Address, opt => opt
                .MapFrom(p => p.AddressId))
                .ForMember(pdto => pdto.Categories, opt => opt
                .MapFrom(p => p.Categories.Select(p => p.Id).ToArray()))
                .ReverseMap();

            CreateMap<ProfileModel, ProfileCreateDTO>()
                .ReverseMap();
        }
    }
}
