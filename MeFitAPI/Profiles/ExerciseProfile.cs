﻿using MeFitAPI.Models.DTOs.Exercise;
using MeFitAPI.Models.Domain;

namespace MeFitAPI.Profiles
{
    public class ExerciseProfile : AutoMapper.Profile
    {
        public ExerciseProfile()
        {
            CreateMap<Exercise, ExerciseReadDTO>()
                .ForMember(edto => edto.Workouts, opt => opt
                .MapFrom(e => e.Workouts.Select(e => e.Id).ToArray()))
                .ForMember(edto => edto.Sets, opt => opt
                .MapFrom(e => e.Sets.Select(e => e.Id).ToArray()))
                .ForMember(edto => edto.Profile, opt => opt
                .MapFrom(e => e.ProfileId))
                .ReverseMap();

            CreateMap<Exercise, ExerciseCreateDTO>()
                .ForMember(edto => edto.Profile, opt => opt
                .MapFrom(e => e.ProfileId))
                .ReverseMap();
        }
    }
}
