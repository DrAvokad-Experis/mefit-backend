﻿using MeFitAPI.Models.Domain;
using MeFitAPI.Models.DTOs.Workout;

namespace MeFitAPI.Profiles
{
    public class WorkoutProfile : AutoMapper.Profile
    {
        public WorkoutProfile()
        {
            CreateMap<Workout, WorkoutReadDTO>()
                .ForMember(wdto => wdto.Exercises, opt => opt
                .MapFrom(w => w.Exercises.Select(w => w.Id).ToArray()))
                .ForMember(wdto => wdto.Programs, opt => opt
                .MapFrom(w => w.Programs.Select(w => w.Id).ToArray()))
                .ForMember(wdto => wdto.Set, opt => opt
                .MapFrom(w => w.Set.Id))
                .ForMember(wdto => wdto.Profile, opt => opt
                .MapFrom(w => w.Profile.Id))
                .ReverseMap();
            CreateMap<Workout, WorkoutCreateDTO>()
                .ForMember(wdto => wdto.Profile, opt => opt
                .MapFrom(w => w.ProfileId))
                .ReverseMap();
        }
    }
}
