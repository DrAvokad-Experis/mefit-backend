﻿using AutoMapper;
using MeFitAPI.Models.Domain;
using MeFitAPI.Models.DTOs.Category;

namespace MeFitAPI.Profiles
{
    public class CategoryProfile : Profile
    {
        public CategoryProfile()
        {
            CreateMap<Category, CategoryReadDTO>()
                 .ForMember(cdto => cdto.Profiles, opt => opt
                 .MapFrom(c => c.Profiles.Select(c => c.Id).ToArray()))
                 .ReverseMap();
        }
    }
}
