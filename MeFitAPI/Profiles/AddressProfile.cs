﻿using AutoMapper;
using MeFitAPI.Models;
using MeFitAPI.Models.DTOs.Address;

namespace MeFitAPI.Profiles
{
    public class AddressProfile : Profile
    {
        public AddressProfile()
        {
            CreateMap<Address, AddressReadDTO>()
                .ForMember(adto => adto.Profile, opt => opt
                .MapFrom(a => a.Profile.Id))
                .ReverseMap();
            CreateMap<AddressCreateDTO, Address>();
        }
    }
}
