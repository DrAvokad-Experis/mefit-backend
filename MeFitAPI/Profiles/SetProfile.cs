﻿using MeFitAPI.Models.Domain;
using MeFitAPI.Models.DTOs.Set;

namespace MeFitAPI.Profiles
{
    public class SetProfile : AutoMapper.Profile
    {
        public SetProfile()
        {
            CreateMap<Set, SetReadDTO>()
               .ForMember(sdto => sdto.Exercise, opt => opt
               .MapFrom(s => s.Exercise.Id))
               .ReverseMap();
            CreateMap<SetCreateDTO, Set>();
        }
    }
}
