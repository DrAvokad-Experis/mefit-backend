<h1 align="center">MeFit Web API</h1>

  <p align="center">
    Noroff Final Case - MeFit
    <br />
    <a href="https://gitlab.com/DrAvokad/mefit-backend/"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://gitlab.com/DrAvokad/mefit-frontend/">Front end repo</a>
    <br />
    <br />
    <a href="https://mefit-noroff-swe.herokuapp.com/">Full application</a>
  </p>
</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#usage">Usage</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>

# About the project
The MeFit application is built with the purpose of letting users keep track of their workout goals. Users can create new goals and add programs, workouts and exercises to the goal. The application also gives the users recommended programs based on their profile information. The full application can be found here: [https://mefit-noroff-swe.herokuapp.com/](https://mefit-noroff-swe.herokuapp.com/).

# Built with
This API was developed with [ASP.NET](https://dotnet.microsoft.com/en-us/apps/aspnet) and [Entity Framework](https://docs.microsoft.com/en-us/aspnet/entity-framework) in the [.Net 6 Core](https://dotnet.microsoft.com/en-us/download/dotnet/6.0) environment. The API is used in the [MeFit web application](https://mefit-noroff-swe.herokuapp.com/)

# Getting Started
To view this project you have to go through these steps:

## Prerequisites
To view this project you will need:
* [Visual Studio](https://visualstudio.microsoft.com/) (preferrable version 2022 since it was used for this project)
* [.Net Core 6](https://dotnet.microsoft.com/en-us/download/dotnet/6.0)
* [ASP.Net](https://dotnet.microsoft.com/en-us/apps/aspnet)
* [Entity Framework](https://docs.microsoft.com/en-us/aspnet/entity-framework)
* A clone of this repo:
```
git clone gitlab.com/DrAvokad/mefit-backend.git
```

## Installation
Packages needed for this project: 
* [EntityFrameworkCore SQLServer](https://www.nuget.org/packages/Microsoft.EntityFrameworkCore.SqlServer/6.0.2)
* [EntityFrameworkCore Design](https://www.nuget.org/packages/Microsoft.EntityFrameworkCore.Design/6.0.2)
* [EntityFrameworkCore Tools](https://www.nuget.org/packages/Microsoft.EntityFrameworkCore.Tools/6.0.2)
* [Automapper](https://www.nuget.org/packages/AutoMapper/11.0.0)
* [ASP.Net JwtBearer](https://www.nuget.org/packages/Microsoft.AspNetCore.Authentication.JwtBearer/)
* [Swashbuckle](https://www.nuget.org/packages/Swashbuckle.AspNetCore/) 
* (Optional) [Visual Studio CodeGeneration](https://www.nuget.org/packages/Microsoft.VisualStudio.Web.CodeGeneration.Design/)

# Usage
When setup is complete, start up the project. You will be redirected to Swagger where you can find all the endpoints.
In the [application](https://mefit-noroff-swe.herokuapp.com/): Log in -> profile -> log key 
Use the key in a service like [Postman](https://www.postman.com/) to access the endpoints

* Some end points will be protected without further authentication *

## Protected 🔒 Endpoints
Every endpoint requires authentication in form of a bearer token. The token is acquired by logging in to the MeFit application at [https://mefit-noroff-swe.herokuapp.com/](https://mefit-noroff-swe.herokuapp.com/).
The endpoints follows the API url: [https://mefit-api.azurewebsites.net/api](https://mefit-api.azurewebsites.net/api)

## Example Endpoints

### MeFit Profiles `GET /Profile/<profile_id>`
#### Sample Response
```json
[
  {
    "id": 1,
    "firstName": "User",
    "lastName": "Userson",
    "height": 180,
    "weight": 86,
    "goalId": 2,
    "goal": "Good goal name",
    "userId": "59e15377-dfa0-4271-b189-a8739fff1f1f",
    "addressId": 53,
    "address": "User street 520",
    "gymPrograms": [1,5,9,14,18],
    "workouts": [5,12,33,42,46,51],
    "exercises": [1,3,4,6,7,8],
    "categories": [1,2,3]
  }
]
```

### MeFit GymPrograms `GET /GymProgram/`
#### Sample Response
```json
[
  {
    "id": 1,
    "name": "Tricep Trifecta",
    "category": "Hypertrophy",
    "workouts": [
      7, 13, 14
    ],
    "profile": null
  },
  {
    "id": 2,
    "name": "Booba Blowup",
    "category": "Strength Training",
    "workouts": [
      8,
      9
    ],
    "profile": null
  },
  {
    "id": 3,
    "name": "Lovely Legs",
    "category": "Hypertrophy",
    "workouts": [
      3,
      6,
      7
    ],
    "profile": null
  }
]
```

### MeFit Goals `POST /Goal/`
#### Sample Body
```json body
{
  "endDate": "2022-03-28T10:35:41.057Z",
  "achieved": false
}
```

### MeFit Assign Workouts to Goal `POST /Goal/<workouts>/<goal_id>`
#### Sample Body
```json body
[
  1, 2, 3
]
```

#### Sample Code
```javascript
const apiURL = 'https://mefit-api.azurewebsites.net/api'
const userId = 'user-id-here'

export const apiGetProfileWithAuth = async (token) => {
  const response = await fetch(`${apiURL}/profile/${userId}`, {
    method: 'GET',
    headers: new Headers({
      'Authorization': 'Bearer ' + token,
      'Content-Type': 'application/json'
    })
  });
  const result = await response.json()

  return result
}
```
# Contact
Contributors: [@achieuw](https://gitlab.com/achieuw) [@DrAvokad](https://gitlab.com/DrAvokad) [@ADrobic](https://gitlab.com/ADrobic)

# Acknowledgements
We want to thank all the lecturers from the [Noroff Acceleration Course](https://www.noroff.no/en/accelerate) for providing fantastic content and making us ready for the next step in our journey.

We also want to thank [Experis Academy](https://www.experis.se/sv/careers/experis-academy-program) for believing in our capabilities as developers and supporting us through the course, aswell as in future endevours.